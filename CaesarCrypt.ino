// includes und Pin Layout:
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);

int tasterLinks = 6;
int tasterRechts = 5;
int tasterEnter = 7;
int slide = 3; 
int reset = 9;

int tasterNormal = 0;

int potiwert = 0;
int potiEingang = A1;

int lcdPos = 0;

int magnet = 13;

// programmspezifische Parameter:

int modus = 0;
int lang = -1;

char alphabet[] = {"abcdefghijklmnopqrstuvwxyz_"};
char de_alphabet[] = {"abcdefghijklmnopqrstuvwxyz"};

String wordlist_predefined = "|fdhvdu?3|pumvythapr?7|datpw?11|mvijtyzvslex?-9|ecbsjb?-2|yinckx?6|";
String custom_word = "";

int w_max;
int word_type = 1;
int count = 1;

// eigene Funktionen:

void resetlcd(int set, String code = "", int part = 0) {
  lcd.clear();
  if(set == -1){
    lcd.print("Sprache/language:");
    }
  if (set == 0) {
    if(lang == 2){lcd.print("Modus waehlen:");}
    else{lcd.print("set mode:");}
  }
  if (set == 1) {
    if (part == 0) {
      if(lang == 2){lcd.print("Text eingeben:");}
      else{lcd.print("enter text:");}
    }
    if (part == -1) {
      if(lang == 2){lcd.print("Symbol 1 leer!");}
      else{lcd.print("1st char empty!");}
    }
    if (part == 1) {
      if(lang == 2){lcd.print("Verschiebung:");}
      else{lcd.print("shift:");}
    }
    if (part == 2) {
      if(lang == 2){lcd.print("<verschluesseln>");}
      else{lcd.print("<encrypting>");}
    }
  }
  if (set == 2) {
    if (part == 1) {
      if(lang == 2){lcd.print("richtig!");}
      else{lcd.print("correct!");}
      return;
    }
    if (part == 2) {
      if(lang == 2){lcd.print("falsch. nochmal!");}
      else{lcd.print("try again!");}
      return;
    }
    lcd.print(code);
  }
  lcd.setCursor(0, 1);
}

void saveWord(String w, int shift) {
  if (custom_word == "") {
    custom_word = "|";
  }
  custom_word = custom_word + w + "?" + shift + "|";
  Serial.println("");
  Serial.print(custom_word);
  Serial.println("");
}

String loadWord(int option, int index) {

  String list;

  if (option == 1) {
    list = wordlist_predefined;
  } else {
    list = custom_word;
  }
  int c = 0;
  bool found = false;
  String word_return = "";
  int part = 0;
  for (int i = 0; i < list.length(); i++) {

    if (list[i] == '|') {
      if (!found) {
        c++;
      }
      else {
        break;
      }
    }
    if (found) {
      word_return += list[i];
    }
    if (c == index) {
      found = true;
    }
  }

  return word_return;

}

void set_w_max() {
  int c = 0;
  for (int i = 0; i < wordlist_predefined.length(); i++) {
    if (wordlist_predefined[i] == '|') {
      c++;
    }
  }

  w_max = c - 1;
}

void setup() {

  lcd.init(); 
  lcd.backlight();

  pinMode(tasterLinks, INPUT);
  pinMode(tasterRechts, INPUT);
  pinMode(tasterEnter, INPUT);
  pinMode(slide, INPUT);

  pinMode(magnet, OUTPUT);
  Serial.begin(9600);
  Serial.print("start"); 

  lcd.begin(16, 2);
  lcd.print("Caesar Crypt");
  delay(3000);

}

void loop() {

  bool nextChar, BLinks, BRechts, BEnter;
  set_w_max();
  lcd.setCursor(modus, 1);

  BEnter = false;

  // Magnet zu
  digitalWrite(magnet, HIGH);

  if (lang == -1) {
    resetlcd(-1);

    int lastmode = 0;
    int mode;
    while (true) {
      if (digitalRead(slide) == LOW) {
        mode = 1;
        if (mode != lastmode) {
          resetlcd(-1);
          lcd.print("Deutsch");
          lastmode = 1;
        }

      }
      else {
        mode = 2;
        if (mode != lastmode) {
          resetlcd(-1);
          lcd.print("Englisch");
          lastmode = 2;
        }
      }
      if (digitalRead(tasterEnter) != tasterNormal) {
        delay(1000);
        break;
      }
    }
    if (digitalRead(slide) == LOW) {
      lang = 2;
    }
    else {
      lang = 1;
    }
  }

  if (modus == 0) {
    resetlcd(0);

    int lastmode = 0;
    int mode;
    while (true) {
      if (digitalRead(slide) == LOW) {
        mode = 1;
        if (mode != lastmode) {
          resetlcd(0);
          if(lang == 2){lcd.print("-Uebungsmodus-");}
          else{lcd.print("-exercise mode-");}
          lastmode = 1;
        }

      }
      else {
        mode = 2;
        if (mode != lastmode) {
          resetlcd(0);
          if(lang == 2){lcd.print("-Tauschmodus-");}
          else{lcd.print("-swap mode-");}
          lastmode = 2;
        }
      }
      if (digitalRead(tasterEnter) != tasterNormal) {
        delay(1000);
        break;
      }
    }
    if (digitalRead(slide) == LOW) {
      modus = 2;
    }
    else {
      modus = 1;
    }
  }

  if (modus == 1) {

    int pos = 0;
    String text = "____________";
    String text2=text;
    char add = '>';
    String decrypted = "";
    String encrypted = "";
    resetlcd(modus);
    lcd.print(text);

    while (!BEnter) {

      nextChar = false;
      BLinks = false;
      BRechts = false;
      int c = 0;

      if(pos>0){add = ' ';}
      if(pos<=0){add = '>';}
      text2 = text;
      text2[pos-1]='>';
      resetlcd(modus);
      lcd.print(add+text2);

      while (!nextChar && !BEnter) {
        int potidiff = analogRead(potiEingang);
        while (abs(potidiff - analogRead(potiEingang)) < 38) {
          if (digitalRead(tasterLinks) != tasterNormal) {
            BLinks = true;
            nextChar = true;
            delay(1000);
            break;
          }
          if (digitalRead(tasterRechts) != tasterNormal) {
            BRechts = true;
            nextChar = true;
            delay(1000);
            break;
          }
          if (digitalRead(tasterEnter) != tasterNormal) {
            if (text[0] != '_') {
              BEnter = true;
            } else {
              resetlcd(modus, "", -1);
            }
            delay(1000);
            break;
          }

          //RESET 
          if(digitalRead(slide)!=HIGH){
            pinMode(reset, OUTPUT);
            digitalWrite(reset, HIGH);
          }
        }
        if (!BLinks && !BRechts && !BEnter) {
          int poti = analogRead(potiEingang) % 1023;
          c = poti / 38;
          resetlcd(modus);
          text[pos] = alphabet[c];
          text2[pos]=text[pos];
          lcd.print(add+text2);
        }
      }
      if (BRechts) {
        if (pos < text.length() - 1) {
          pos++;
        }
      }
      if (BLinks) {
        if (pos > 0) {
          pos--;
        }
      }
      nextChar = false;
      BLinks = false;
      BRechts = false;
    }

    for (int i = 0; i < text.length(); i++) {
      if (text[i] != '_') {
        decrypted += text[i];
      } else {
        if (i + 1 < text.length()) {
          if (text[i + 1] == '_') {
            break;
          }
          decrypted += " ";
        } else {
          break;
        }
      }
    }
    BEnter = false;
    resetlcd(modus, "", 1);
    lcd.print("___");

    int digit = 0;
    while (!BEnter) {
      int potidiff = analogRead(potiEingang);
      while (abs(potidiff - analogRead(potiEingang)) < 20) {
        if (digitalRead(tasterEnter) != tasterNormal) {
          BEnter = true;
          delay(1000);
          break;
        }
        //RESET 
        if(digitalRead(slide)!=HIGH){
          pinMode(reset, OUTPUT);
          digitalWrite(reset, HIGH);
        }
      }
      if (!BEnter) {
        int poti = analogRead(potiEingang) % 1023;
        digit = (poti / 20) - 25;
        String text = String(digit);
        if(digit>=0){text="+"+text;}
        if(text.length()<3){text+="_";}
        resetlcd(modus, "", 1);
        lcd.print(text);

      }
    }

    BEnter = false;

    resetlcd(modus, "", 2);
    delay(1000);
    int buffer = 0;
    for (int i = 0; i < decrypted.length(); i++) {
      if (decrypted[i] == ' ') {
        encrypted += " ";
      }
      for (int j = 0; j < 26; j++) {
        if (alphabet[j] == decrypted[i]) {
          buffer = j + digit;
          if (buffer > 26) {
            buffer = buffer - 26;
          }
          if (buffer < 0) {
            buffer = buffer + 26;
          }
          encrypted += de_alphabet[buffer];
        }
      }
    }
    saveWord(encrypted, digit);
    modus = 2;
    word_type = 2;

  }
  if (modus == 2) {

    int pos = 0;
    String crypt_full = loadWord(word_type, count);

    String crypt_word = "";
    int solution_shift = 0;

    int i = 0;
    while (crypt_full[i] != '?') {
      crypt_word += crypt_full[i];
      i++;
    }

    solution_shift = crypt_full.substring(i + 1, crypt_full.length()).toInt();

    String text;
    String text2;
    char add = '>';
    String probe;
    String output;
    bool korrekt = false;

    while (!korrekt) {

      BEnter = false;

      output = crypt_word + " ";
      if (word_type == 1 && count < 4) {
        output += solution_shift;
      }
      resetlcd(modus, output);

      text = "";
      for (int i = 0; i < crypt_word.length(); i++) {
        text += "_";
      }

      lcd.print(text);

      while (!BEnter) {

        nextChar = false;
        BLinks = false;
        BRechts = false;
        int c = 0;

        if(pos>0){add = ' ';}
        if(pos<=0){add = '>';}
        text2 = text;
        text2[pos-1]='>';
        resetlcd(modus, output);
        lcd.print(add+text2);

        while (!nextChar && !BEnter) {
          int potidiff = analogRead(potiEingang);
          while (abs(potidiff - analogRead(potiEingang)) < 38) {
            if (digitalRead(tasterLinks) != tasterNormal) {
              BLinks = true;
              nextChar = true;
              delay(1000);
              break;
            }
            if (digitalRead(tasterRechts) != tasterNormal) {
              BRechts = true;
              nextChar = true;
              delay(1000);
              break;
            }
            if (digitalRead(tasterEnter) != tasterNormal) {
              BEnter = true;
              delay(1000);
              break;
            }

            // RESET
            if(digitalRead(slide)!=LOW and word_type==1){
              pinMode(reset, OUTPUT);
              digitalWrite(reset, HIGH);
            }
            if(digitalRead(slide)!=HIGH and word_type==2){
              pinMode(reset, OUTPUT);
              digitalWrite(reset, HIGH);
            }
          }
          if (!BLinks && !BRechts && !BEnter) {
            int poti = analogRead(potiEingang) % 1023;
            c = poti / 38;
            resetlcd(modus, output);
            text[pos] = alphabet[c];
            text2[pos]=text[pos];
            lcd.print(add+text2);
          }
        }
        if (BRechts) {
          if (pos < text.length() - 1) {
            pos++;
          }
        }
        if (BLinks) {
          if (pos > 0) {
            pos--;
          }
        }
        nextChar = false;
        BLinks = false;
        BRechts = false;
      }
      for (int i = 0; i < text.length(); i++) {
        if (text[i] == '_') {
          text[i] = ' ';
        }
      }

      probe = "";
      int buffer = 0;
      for (int i = 0; i < crypt_word.length(); i++) {
        if (crypt_word[i] == ' ') {
          probe += " ";
        }
        for (int j = 0; j < 26; j++) {
          if (alphabet[j] == crypt_word[i]) {
            buffer = j - solution_shift;
            if (buffer > 26) {
              buffer = buffer - 26;
            }
            if (buffer < 0) {
              buffer = buffer + 26;
            }
            probe += de_alphabet[buffer];
          }
        }
      }
      if (text == probe) {
        resetlcd(modus, "", 1);
        korrekt = true;

        // Hier Magnet auf
        digitalWrite(magnet, LOW);
        delay(3000); 

        if (word_type == 1) {
          if (count < w_max) {
            count++;
          } else {
            count = 1;
          }
        }
        else {
          count++;
        }
        delay(3000);
      } else {
        resetlcd(modus, "", 2);
        pos = 0;
        delay(1000);
      }

    }

    if (word_type == 2) {
      modus = 1;
    }

  }



}
