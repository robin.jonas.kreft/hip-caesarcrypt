# HIP - CaesarCrypt 

Hier leigt der Code und weitere Informationen/Anleitungen für unser Arduino Projekt zum Thema Kryptografie.

- Zur Nutzung der Software muss die [Arduino IDE](https://www.arduino.cc/en/software) installiert sein. Darüberhinaus wird auch noch eine Bibliothek zur Steuerung des Displays via I2C-Schnittstelle benötigt. Diese findet man [hier](https://www.arduino.cc/reference/en/libraries/liquidcrystal-i2c/).

- Der Konstruktionsplan enthält Informationen zu den einzelnen Kompnenten, die wir verwendet haben. Andere Konfigurationen, insbesondere des Aussehens, sind aber ebenfalls möglich. Für unsere Holz-Komponenten wurde eines der [hier](https://www.festi.info/boxes.py/) zu findenden Templates genutzt. 
